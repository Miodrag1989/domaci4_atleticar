﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace domaci5_atleticar
{
    class Program
    {
        static void Main(string[] args)
        {
            Disciplina[] d = new Disciplina[2];

            d[0] = new Disciplina("110m s preponama", Tip.Trkacka, 2);
            d[0].UcitavanjeUcesnika();
            d[0].Pobednik();

            d[1] = new Disciplina("skok u dalj", Tip.Skakacka, 2);
            d[1].UcitavanjeUcesnika();
            
            d[1].Pobednik();
        }
    }
}
