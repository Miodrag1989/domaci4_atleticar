﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace domaci5_atleticar
{
    class Disciplina
    {
        private string imeDiscipline;
        private Tip tip;
        private int brojUcesnika;
        private Atleticar[] nizAtleticara;

        public Disciplina(string imeDiscipline, Tip tip, int brojUcesnika)
        {
            this.imeDiscipline = imeDiscipline;
            this.tip = tip;
            this.brojUcesnika = brojUcesnika;
            this.nizAtleticara = new Atleticar[brojUcesnika];
            
        }

        public void UcitavanjeUcesnika()
        {
            for (int i = 0; i < nizAtleticara.Length; i++)
            {
                if (this.tip == Tip.Skakacka)
                {
                    nizAtleticara[i] = new Skakac();
                    nizAtleticara[i].Ucitavanje();
                }
                else
                {
                    nizAtleticara[i] = new Trkac();
                    nizAtleticara[i].Ucitavanje();
                }

            }
        }

        public void Pobednik()
        {
            Atleticar a;
            if(this.tip == Tip.Skakacka)
            {
                 a = new Skakac();
            }
            else
            {
                 a = new Trkac();
            }
           
            int index = a.Provera(nizAtleticara);
            Console.WriteLine($"Pobednik discipline {this.imeDiscipline} je: ");
            nizAtleticara[index].Prikazivanje();
        }
    }
}
