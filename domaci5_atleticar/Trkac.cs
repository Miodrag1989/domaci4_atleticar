﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace domaci5_atleticar
{
    class Trkac : Atleticar
    {
        
        public override int Provera(Atleticar[] nizAtleticara)
        {
            int index = 0;
            for (int i = 1; i < nizAtleticara.Length; i++)
            {
                if (nizAtleticara[i].Rezultat < nizAtleticara[index].Rezultat)
                {
                    index = i;
                }
            }
            return index;
        }
        
    }
}
