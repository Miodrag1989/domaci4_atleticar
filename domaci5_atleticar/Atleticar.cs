﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace domaci5_atleticar
{
    public abstract class Atleticar
    {
        private string ime;
        private string prezime;
        protected float rezultat;

        public void Ucitavanje()
        {
            Console.WriteLine("Ime:");
            this.ime = Console.ReadLine();
            Console.WriteLine("Prezime:");
            this.prezime = Console.ReadLine();
            Console.WriteLine("Postignut rezultat:");
            this.rezultat = float.Parse(Console.ReadLine());
        }


        public abstract int Provera(Atleticar[] nizAtleticara);

        public void Prikazivanje()
        {
            //Console.WriteLine("Aleticar:");
            Console.WriteLine($"Ime: {this.ime} Prezime: {this.prezime} Rezultat: {this.rezultat}");
        }

        public float Rezultat{ get { return this.rezultat; } }
        
    }
}
